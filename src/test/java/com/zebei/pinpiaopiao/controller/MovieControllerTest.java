package com.zebei.pinpiaopiao.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.zebei.pinpiaopiao.dto.MovieRequest;
import com.zebei.pinpiaopiao.entity.Movie;
import com.zebei.pinpiaopiao.repository.MovieRepository;
import lombok.RequiredArgsConstructor;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class MovieControllerTest {

    @Autowired
    private MovieRepository movieRepository;

    private final ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    private MockMvc client;

    @BeforeEach
    void setUp() {
        movieRepository.deleteAll();
    }

    @Test
    void should_return_movie_list_when_perform_get_all_given_movies() throws Exception {
        List<Movie> movies = new ArrayList<>();
        Movie movie1 = buildMovieFenShen1();
        Movie movie2 = buildMovieFenShen2();
        movies.add(movie1);
        movies.add(movie2);
        movieRepository.saveAll(List.of(movie1,movie2));

        client.perform(MockMvcRequestBuilders.get("/movies"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].name").value("封神第一部"))
                .andExpect(jsonPath("$[0].director").value("乌尔善"))
                .andExpect(jsonPath("$[1].name").value("封神第二部"))
                .andExpect(jsonPath("$[1].director").value("乌尔善"));

    }
    @Test
    void should_return_movie_list_with_key_when_perform_get_all_given_movies() throws Exception {
        List<Movie> movies = new ArrayList<>();
        Movie movie1 = buildMovieFenShen1();
        Movie movie2 = buildMovieFenShen2();
        movies.add(movie1);
        movies.add(movie2);
        List<Movie> moviesSaved = movieRepository.saveAll(List.of(movie1, movie2));

        client.perform(MockMvcRequestBuilders.get("/movies/key"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].key").value(moviesSaved.get(0).getId()))
                .andExpect(jsonPath("$[0].name").value("封神第一部"))
                .andExpect(jsonPath("$[0].director").value("乌尔善"))
                .andExpect(jsonPath("$[1].key").value(moviesSaved.get(1).getId()))
                .andExpect(jsonPath("$[1].name").value("封神第二部"))
                .andExpect(jsonPath("$[1].director").value("乌尔善"));

    }

    @Test
    void should_return_movie_when_perform_add_given_movies() throws Exception {
        MovieRequest movieRequest = buildMovieRequestFenShen1();

        client.perform(MockMvcRequestBuilders.post("/movies")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(movieRequest)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.name").value(movieRequest.getName()))
                .andExpect(jsonPath("$.director").value(movieRequest.getDirector()));

        List<Movie> movies = movieRepository.findAll();
        assertThat(movies, hasSize(1));
        assertThat(movies.get(0).getName(), equalTo(movieRequest.getName()));
        assertThat(movies.get(0).getType(), equalTo(movieRequest.getType()));


    }

    @Test
    void should_return_movie_when_perform_update_given_movie() throws Exception {
        MovieRequest movieRequest = buildMovieRequestFenShen1();
        Movie movie = buildMovieFenShen2();
        Movie save = movieRepository.save(movie);

        client.perform(MockMvcRequestBuilders.put("/movies/{id}",1)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(movieRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value(movieRequest.getName()))
                .andExpect(jsonPath("$.director").value(movieRequest.getDirector()));

        List<Movie> movies = movieRepository.findAll();
        assertThat(movies, hasSize(1));
        assertThat(movies.get(0).getName(), equalTo(movieRequest.getName()));
        assertThat(movies.get(0).getType(), equalTo(movieRequest.getType()));


    }

    @Test
    void should_return_nothing_when_perform_delete_given_movie() throws Exception {
        Movie movie = buildMovieFenShen2();
        Movie save = movieRepository.save(movie);

        client.perform(MockMvcRequestBuilders.delete("/movies/{id}",save.getId()))
                .andExpect(status().isOk());

        Assertions.assertNull(movieRepository.findById(save.getId()).orElse(null));

    }

    public MovieRequest buildMovieRequestFenShen1() throws ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        return new MovieRequest("封神第一部", "神话", "https://img.alicdn.com/bao/uploaded/i3/O1CN01iSbMsa1eAeBlh1OKe_!!6000000003831-0-alipicbeacon.jpg_300x300.jpg","剧情介绍：中国国民神话史诗大片，震撼视效燃炸暑期！《封神第一部》讲述商王殷寿勾结狐妖妲己，暴虐无道，引发天谴。昆仑仙人姜子牙携“封神榜”下山，寻找天下共主，以救苍生。西伯侯之子姬发逐渐发现殷寿的本来面目，反出朝歌。各方势力暗流涌动，风云变幻端倪初显……三千年想象成真，恢弘史诗再创新，见证中国神话崛起！","乌尔善","费翔,李雪健", dateFormat.parse("2023-07-20"),  148);
    }
    public Movie buildMovieFenShen1() throws ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        return new Movie("封神第一部", "神话", "https://img.alicdn.com/bao/uploaded/i3/O1CN01iSbMsa1eAeBlh1OKe_!!6000000003831-0-alipicbeacon.jpg_300x300.jpg","剧情介绍：中国国民神话史诗大片，震撼视效燃炸暑期！《封神第一部》讲述商王殷寿勾结狐妖妲己，暴虐无道，引发天谴。昆仑仙人姜子牙携“封神榜”下山，寻找天下共主，以救苍生。西伯侯之子姬发逐渐发现殷寿的本来面目，反出朝歌。各方势力暗流涌动，风云变幻端倪初显……三千年想象成真，恢弘史诗再创新，见证中国神话崛起！","乌尔善","费翔,李雪健", dateFormat.parse("2023-07-20"),  148);
    }

    public Movie buildMovieFenShen2() throws ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        return new Movie("封神第二部", "神话", "https://img.alicdn.com/bao/uploaded/i3/O1CN01iSbMsa1eAeBlh1OKe_!!6000000003831-0-alipicbeacon.jpg_300x300.jpg","剧情介绍：中国国民神话史诗大片，震撼视效燃炸暑期！《封神第一部》讲述商王殷寿勾结狐妖妲己，暴虐无道，引发天谴。昆仑仙人姜子牙携“封神榜”下山，寻找天下共主，以救苍生。西伯侯之子姬发逐渐发现殷寿的本来面目，反出朝歌。各方势力暗流涌动，风云变幻端倪初显……三千年想象成真，恢弘史诗再创新，见证中国神话崛起！","乌尔善","费翔,李雪健", dateFormat.parse("2023-07-20"),  148);
    }




}
