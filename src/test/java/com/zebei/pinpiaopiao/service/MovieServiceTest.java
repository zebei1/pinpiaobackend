package com.zebei.pinpiaopiao.service;

import com.zebei.pinpiaopiao.dto.MovieRequest;
import com.zebei.pinpiaopiao.dto.MovieResponse;
import com.zebei.pinpiaopiao.dto.MovieResponseKey;
import com.zebei.pinpiaopiao.entity.Movie;
import com.zebei.pinpiaopiao.exception.MovieNotFoundException;
import com.zebei.pinpiaopiao.mapper.MovieMapper;
import com.zebei.pinpiaopiao.repository.MovieRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.*;

public class MovieServiceTest {
    private final MovieRepository movieRepository = mock(MovieRepository.class);
    private final MovieService movieService = new MovieService(movieRepository);

    @BeforeEach
    void setUp() {
        movieRepository.deleteAll();
    }

    @Test
    void should_return_all_movies_when_perform_query_all_movie_given_movies() throws ParseException {
        List<Movie> movies = new ArrayList<>();
        Movie movie1 = buildMovieFenShen1();
        Movie movie2 = buildMovieFenShen2();
        movies.add(movie1);
        movies.add(movie2);

        when(movieRepository.findAll()).thenReturn(movies);
        List<MovieResponse> result = movieService.findAll();

        verify(movieRepository, times(1)).findAll();
        Assertions.assertEquals(2, result.size());
        Assertions.assertEquals(movie1.getId(),result.get(0).getId());
        Assertions.assertEquals(movie2.getId(),result.get(1).getId());

    }

    @Test
    void should_return_all_movies_with_key_when_perform_query_all_movie_given_movies() throws ParseException {
        List<Movie> movies = new ArrayList<>();
        Movie movie1 = buildMovieFenShen1();
        Movie movie2 = buildMovieFenShen2();
        movies.add(movie1);
        movies.add(movie2);

        when(movieRepository.findAll()).thenReturn(movies);
        List<MovieResponseKey> result = movieService.findAllWithKey();

        verify(movieRepository, times(1)).findAll();
        Assertions.assertEquals(2, result.size());
        Assertions.assertEquals(movie1.getId(),result.get(0).getKey());
        Assertions.assertEquals(movie2.getId(),result.get(1).getKey());

    }

    @Test
    void should_return_saved_movie_when_perform_insert_movie_given_movie() throws ParseException {
        MovieRequest movieRequest = buildMovieRequestFenShen1();
        Movie movie = MovieMapper.movieRequestToMovie(movieRequest);

        when(movieRepository.save(any())).thenReturn(movie);
        MovieResponse movieResponse = movieService.insertMovie(movieRequest);

        verify(movieRepository, times(1)).save(any());
        Assertions.assertEquals(movie.getName(),movieResponse.getName());

    }

    @Test
    void should_return_updated_movie_when_perform_update_movie_given_movie() throws ParseException, MovieNotFoundException {
        MovieRequest movieRequest = buildMovieRequestFenShen1();
        Movie movie = buildMovieFenShen1();

        when(movieRepository.findById(any())).thenReturn(Optional.ofNullable(movie));
        when(movieRepository.save(any())).thenReturn(movie);
        MovieResponse movieResponse = movieService.updateMovie(2,movieRequest);

        verify(movieRepository, times(1)).save(any());
        verify(movieRepository, times(1)).findById(any());
        Assertions.assertEquals(movie.getName(),movieResponse.getName());

    }

    @Test
    void should_return_nothing_when_perform_delete_movie_given_movie() throws ParseException {

        movieService.removeMovie(2);
        verify(movieRepository, times(1)).deleteById(any());
    }

    public MovieRequest buildMovieRequestFenShen1() throws ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        return new MovieRequest("封神第一部", "神话", "https://img.alicdn.com/bao/uploaded/i3/O1CN01iSbMsa1eAeBlh1OKe_!!6000000003831-0-alipicbeacon.jpg_300x300.jpg","剧情介绍：中国国民神话史诗大片，震撼视效燃炸暑期！《封神第一部》讲述商王殷寿勾结狐妖妲己，暴虐无道，引发天谴。昆仑仙人姜子牙携“封神榜”下山，寻找天下共主，以救苍生。西伯侯之子姬发逐渐发现殷寿的本来面目，反出朝歌。各方势力暗流涌动，风云变幻端倪初显……三千年想象成真，恢弘史诗再创新，见证中国神话崛起！","乌尔善","费翔,李雪健", dateFormat.parse("2023-07-20"),  148);
    }
    public Movie buildMovieFenShen1() throws ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        return new Movie(1,"封神第一部", "神话", "https://img.alicdn.com/bao/uploaded/i3/O1CN01iSbMsa1eAeBlh1OKe_!!6000000003831-0-alipicbeacon.jpg_300x300.jpg","剧情介绍：中国国民神话史诗大片，震撼视效燃炸暑期！《封神第一部》讲述商王殷寿勾结狐妖妲己，暴虐无道，引发天谴。昆仑仙人姜子牙携“封神榜”下山，寻找天下共主，以救苍生。西伯侯之子姬发逐渐发现殷寿的本来面目，反出朝歌。各方势力暗流涌动，风云变幻端倪初显……三千年想象成真，恢弘史诗再创新，见证中国神话崛起！","乌尔善","费翔,李雪健", dateFormat.parse("2023-07-20"),  148);
    }

    public Movie buildMovieFenShen2() throws ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        return new Movie(2,"封神第二部", "神话", "https://img.alicdn.com/bao/uploaded/i3/O1CN01iSbMsa1eAeBlh1OKe_!!6000000003831-0-alipicbeacon.jpg_300x300.jpg","剧情介绍：中国国民神话史诗大片，震撼视效燃炸暑期！《封神第一部》讲述商王殷寿勾结狐妖妲己，暴虐无道，引发天谴。昆仑仙人姜子牙携“封神榜”下山，寻找天下共主，以救苍生。西伯侯之子姬发逐渐发现殷寿的本来面目，反出朝歌。各方势力暗流涌动，风云变幻端倪初显……三千年想象成真，恢弘史诗再创新，见证中国神话崛起！","乌尔善","费翔,李雪健", dateFormat.parse("2023-07-20"),  148);
    }

}
