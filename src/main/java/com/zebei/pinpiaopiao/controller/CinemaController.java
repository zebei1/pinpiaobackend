package com.zebei.pinpiaopiao.controller;

import com.zebei.pinpiaopiao.dto.CinemaRequest;
import com.zebei.pinpiaopiao.dto.CinemaResponse;
import com.zebei.pinpiaopiao.exception.CinemaNotFoundException;
import com.zebei.pinpiaopiao.service.CinemaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/cinemas")
public class CinemaController {
    @Autowired
    private final CinemaService cinemaService;

    public CinemaController(CinemaService cinemaService) {
        this.cinemaService = cinemaService;
    }

    @GetMapping
    public List<CinemaResponse> getAll(){
        return cinemaService.finAll();
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public CinemaResponse addCinema(@RequestBody CinemaRequest cinemaRequest){
        return cinemaService.insertCinema(cinemaRequest);
    }

    @PutMapping("/{id}")
    public CinemaResponse editCinema(@PathVariable Integer id,@RequestBody CinemaRequest cinemaRequest) throws CinemaNotFoundException {
        return cinemaService.updateCinema(id,cinemaRequest);
    }

    @DeleteMapping("/{id}")
    public void deleteCinema(@PathVariable Integer id){
        cinemaService.removeCinema(id);
    }
}
