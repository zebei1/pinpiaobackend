package com.zebei.pinpiaopiao.controller;

import com.zebei.pinpiaopiao.dto.MovieRequest;
import com.zebei.pinpiaopiao.dto.MovieResponse;
import com.zebei.pinpiaopiao.dto.MovieResponseKey;
import com.zebei.pinpiaopiao.exception.MovieNotFoundException;
import com.zebei.pinpiaopiao.service.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/movies")
public class MovieController {
    @Autowired
    private final MovieService movieService;

    public MovieController(MovieService movieService) {
        this.movieService = movieService;
    }

    @GetMapping
    public List<MovieResponse> getAll(){
        return movieService.findAll();
    }

    @GetMapping("/key")
    public List<MovieResponseKey> getAllWithKey(){
        return movieService.findAllWithKey();
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public MovieResponse addMovie(@RequestBody MovieRequest movieRequest){
        return movieService.insertMovie(movieRequest);
    }

    @PutMapping("/{id}")
    public MovieResponse editMovie(@PathVariable Integer id, @RequestBody MovieRequest movieRequest) throws MovieNotFoundException {
        return movieService.updateMovie(id,movieRequest);
    }

    @DeleteMapping("/{id}")
    public void deleteMovie(@PathVariable Integer id){
        movieService.removeMovie(id);
    }
}
