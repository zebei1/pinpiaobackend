package com.zebei.pinpiaopiao.mapper;

import com.zebei.pinpiaopiao.dto.CinemaRequest;
import com.zebei.pinpiaopiao.dto.CinemaResponse;
import com.zebei.pinpiaopiao.entity.Cinema;
import org.springframework.beans.BeanUtils;

public class CinemaMapper {
    public static CinemaResponse toResponse(Cinema cinema){
        CinemaResponse cinemaResponse = new CinemaResponse();
        BeanUtils.copyProperties(cinema, cinemaResponse);
        return cinemaResponse;
    }

    public static Cinema toEntity (CinemaRequest cinemaRequest){
        Cinema cinema = new Cinema();
        BeanUtils.copyProperties(cinemaRequest,cinema);
        return cinema;
    }
}
