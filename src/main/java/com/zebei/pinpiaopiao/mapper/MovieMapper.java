package com.zebei.pinpiaopiao.mapper;
import com.zebei.pinpiaopiao.dto.MovieRequest;
import com.zebei.pinpiaopiao.dto.MovieResponse;
import com.zebei.pinpiaopiao.dto.MovieResponseKey;
import com.zebei.pinpiaopiao.entity.Movie;
import org.springframework.beans.BeanUtils;


public class MovieMapper {
    public static MovieResponse toResponse(Movie movie){
        MovieResponse movieResponse = new MovieResponse();
        BeanUtils.copyProperties(movie, movieResponse);
        return movieResponse;
    }

    public static MovieResponseKey toResponseReplaceIdToKey(Movie movie){
        MovieResponseKey movieResponseKey = new MovieResponseKey();
        BeanUtils.copyProperties(movie, movieResponseKey);
        movieResponseKey.setKey(movie.getId());
        return movieResponseKey;
    }

    public static Movie movieRequestToMovie(MovieRequest movieRequest){
        Movie movie = new Movie();
        BeanUtils.copyProperties(movieRequest, movie);
        return movie;
    }

}
