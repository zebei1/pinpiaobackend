package com.zebei.pinpiaopiao.dto;

public class CinemaResponse {
    private Integer id;
    private String cinemaName;
    private String address;
    private String city;

    public CinemaResponse() {
    }

    public CinemaResponse(Integer id, String cinemaName, String address, String city) {
        this.id = id;
        this.cinemaName = cinemaName;
        this.address = address;
        this.city = city;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCinemaName() {
        return cinemaName;
    }

    public void setCinemaName(String cinemaName) {
        this.cinemaName = cinemaName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
