package com.zebei.pinpiaopiao.dto;

import java.util.Date;

public class MovieResponseKey {
    private Integer key;
    private String name;
    private String type;
    private String imgUrl;
    private String description;
    private String director;
    private String actors;
    private Date releaseDate;
    private Integer duration;

    public MovieResponseKey() {
    }

    public MovieResponseKey(Integer key, String name, String type, String imgUrl, String description, String director, String actors, Date releaseDate, Integer duration) {
        this.key = key;
        this.name = name;
        this.type = type;
        this.imgUrl = imgUrl;
        this.description = description;
        this.director = director;
        this.actors = actors;
        this.releaseDate = releaseDate;
        this.duration = duration;
    }

    public MovieResponseKey(String name, String type, String imgUrl, String description, String director, String actors, Date releaseDate, Integer duration) {
        this.name = name;
        this.type = type;
        this.imgUrl = imgUrl;
        this.description = description;
        this.director = director;
        this.actors = actors;
        this.releaseDate = releaseDate;
        this.duration = duration;
    }

    public Integer getKey() {
        return key;
    }

    public void setKey(Integer key) {
        this.key = key;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getActors() {
        return actors;
    }

    public void setActors(String actors) {
        this.actors = actors;
    }

    public Date getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }
}
