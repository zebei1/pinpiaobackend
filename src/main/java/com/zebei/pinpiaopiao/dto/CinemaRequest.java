package com.zebei.pinpiaopiao.dto;

public class CinemaRequest {
    private String cinemaName;
    private String address;
    private String city;

    public CinemaRequest() {
    }

    public CinemaRequest(String cinemaName, String address, String city) {
        this.cinemaName = cinemaName;
        this.address = address;
        this.city = city;
    }

    public String getCinemaName() {
        return cinemaName;
    }

    public void setCinemaName(String cinemaName) {
        this.cinemaName = cinemaName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
