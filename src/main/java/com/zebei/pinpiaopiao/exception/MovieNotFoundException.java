package com.zebei.pinpiaopiao.exception;

public class MovieNotFoundException extends  Exception {
    public MovieNotFoundException() {
        super("Movie is not found!");
    }
}
