package com.zebei.pinpiaopiao.exception;

public class CinemaNotFoundException extends Exception{
    public CinemaNotFoundException() {
        super("Cinema is not found!");
    }
}
