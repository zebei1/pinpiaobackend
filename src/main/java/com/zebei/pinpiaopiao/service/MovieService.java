package com.zebei.pinpiaopiao.service;

import com.zebei.pinpiaopiao.dto.MovieRequest;
import com.zebei.pinpiaopiao.dto.MovieResponse;
import com.zebei.pinpiaopiao.dto.MovieResponseKey;
import com.zebei.pinpiaopiao.entity.Movie;
import com.zebei.pinpiaopiao.exception.MovieNotFoundException;
import com.zebei.pinpiaopiao.mapper.MovieMapper;
import com.zebei.pinpiaopiao.repository.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class MovieService {

    @Autowired
    private final MovieRepository movieRepository;

    public MovieService(MovieRepository movieRepository) {
        this.movieRepository = movieRepository;
    }

    public List<MovieResponse> findAll() {
        return movieRepository.findAll().stream().map(MovieMapper::toResponse).collect(Collectors.toList());
    }

    public List<MovieResponseKey> findAllWithKey() {
        return movieRepository.findAll().stream().map(MovieMapper::toResponseReplaceIdToKey).collect(Collectors.toList());
    }

    public MovieResponse insertMovie(MovieRequest movieRequest) {
        return MovieMapper.toResponse(movieRepository.save(MovieMapper.movieRequestToMovie(movieRequest)));
    }

    public MovieResponse updateMovie(Integer id ,MovieRequest movieRequest) throws MovieNotFoundException {
        Movie movieToUpdate = movieRepository.findById(id).orElseThrow(MovieNotFoundException::new);
        movieToUpdate.setName(movieRequest.getName());
        movieToUpdate.setActors(movieRequest.getActors());
        movieToUpdate.setDescription(movieRequest.getDescription());
        movieToUpdate.setDirector(movieRequest.getDirector());
        movieToUpdate.setDuration(movieRequest.getDuration());
        movieToUpdate.setImgUrl(movieRequest.getImgUrl());
        movieToUpdate.setReleaseDate(movieRequest.getReleaseDate());
        movieToUpdate.setType(movieRequest.getType());
        return MovieMapper.toResponse(movieRepository.save(movieToUpdate));
    }

    public void removeMovie(Integer id) {
         movieRepository.deleteById(id);
    }
}
