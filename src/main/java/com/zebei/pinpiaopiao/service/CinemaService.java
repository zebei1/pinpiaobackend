package com.zebei.pinpiaopiao.service;

import com.zebei.pinpiaopiao.dto.CinemaRequest;
import com.zebei.pinpiaopiao.dto.CinemaResponse;
import com.zebei.pinpiaopiao.entity.Cinema;
import com.zebei.pinpiaopiao.exception.CinemaNotFoundException;
import com.zebei.pinpiaopiao.mapper.CinemaMapper;
import com.zebei.pinpiaopiao.repository.CinemaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CinemaService {
    @Autowired
    private final CinemaRepository cinemaRepository;

    public CinemaService(CinemaRepository cinemaRepository) {
        this.cinemaRepository = cinemaRepository;
    }

    public List<CinemaResponse> finAll(){
        return cinemaRepository.findAll().stream().map(CinemaMapper::toResponse).collect(Collectors.toList());
    }

    public CinemaResponse insertCinema(CinemaRequest cinemaRequest){
        return CinemaMapper.toResponse(cinemaRepository.save(CinemaMapper.toEntity(cinemaRequest)));
    }


    public CinemaResponse updateCinema(Integer id, CinemaRequest cinemaRequest) throws CinemaNotFoundException {
        Cinema cinemaToUpdate = cinemaRepository.findById(id).orElseThrow(CinemaNotFoundException::new);
        cinemaToUpdate.setCinemaName(cinemaRequest.getCinemaName());
        cinemaToUpdate.setCity(cinemaRequest.getCity());
        cinemaToUpdate.setAddress(cinemaRequest.getAddress());
        return CinemaMapper.toResponse(cinemaRepository.save(cinemaToUpdate));
    }

    public void removeCinema(Integer id) {
        cinemaRepository.deleteById(id);
    }
}
