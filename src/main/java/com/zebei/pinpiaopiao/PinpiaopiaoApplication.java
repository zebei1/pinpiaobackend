package com.zebei.pinpiaopiao;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PinpiaopiaoApplication {

	public static void main(String[] args) {
		SpringApplication.run(PinpiaopiaoApplication.class, args);
	}

}
