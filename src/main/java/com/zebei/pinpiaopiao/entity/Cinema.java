package com.zebei.pinpiaopiao.entity;

import javax.persistence.*;

@Entity
@Table(name = "t_cinema")
public class Cinema {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String cinemaName;
    private String address;
    private String city;

    public Cinema() {
    }

    public Cinema(Integer id, String cinemaName, String address, String city) {
        this.id = id;
        this.cinemaName = cinemaName;
        this.address = address;
        this.city = city;
    }

    public Cinema(String cinemaName, String address, String city) {
        this.cinemaName = cinemaName;
        this.address = address;
        this.city = city;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCinemaName() {
        return cinemaName;
    }

    public void setCinemaName(String cinemaName) {
        this.cinemaName = cinemaName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
