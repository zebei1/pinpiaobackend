package com.zebei.pinpiaopiao.repository;

import com.zebei.pinpiaopiao.entity.Cinema;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CinemaRepository extends JpaRepository<Cinema,Integer> {
}
