DROP TABLE IF EXISTS `t_hall`;

CREATE TABLE `t_hall` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '唯一id',
  `cinema_id` int NOT NULL COMMENT '电影院id',
  `name` varchar(100)   DEFAULT NULL COMMENT '名称',
  `seat_count` int DEFAULT '0' COMMENT '座位数',
  PRIMARY KEY (`id`)
);

DROP TABLE IF EXISTS `t_order`;

CREATE TABLE `t_order` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '唯一id',
  `user_id` int NOT NULL COMMENT '用户id',
  `hall_id` int NOT NULL COMMENT '放映厅id',
  `session_id` int NOT NULL COMMENT '场次id',
  `seat_number` varchar(55) DEFAULT NULL COMMENT '座位号',
  `status` tinyint DEFAULT NULL COMMENT '订单状态',
  PRIMARY KEY (`id`)
);

DROP TABLE IF EXISTS `t_seat`;

CREATE TABLE `t_seat` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '唯一id',
  `seat_id` int NOT NULL COMMENT '座位id',
  `hall_id` int NOT NULL COMMENT '放映厅id',
  `seat_row` int NOT NULL COMMENT '行',
  `seat_col` int NOT NULL COMMENT '列',
  `seat_number` varchar(55)  DEFAULT NULL COMMENT '座位号',
  `status` tinyint DEFAULT NULL COMMENT '可选，已选，已售',
  PRIMARY KEY (`id`)
);


DROP TABLE IF EXISTS `t_session`;

CREATE TABLE `t_session` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '唯一id',
  `hall_id` int NOT NULL COMMENT '放映厅id',
  `movie_id` int NOT NULL COMMENT '电影id',
  `start_time` datetime DEFAULT NULL COMMENT '开始时间',
  `end_time` datetime DEFAULT NULL COMMENT '结束时间',
  PRIMARY KEY (`id`)
);
