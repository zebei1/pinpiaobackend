DROP TABLE IF EXISTS `t_cinema`;

CREATE TABLE `t_cinema` (
`id` int NOT NULL AUTO_INCREMENT COMMENT '唯一id',
`cinema_name` varchar(200)   DEFAULT NULL COMMENT '名称',
`address` varchar(200)   DEFAULT NULL COMMENT '地址',
`city` varchar(50)   DEFAULT NULL COMMENT '城市',
PRIMARY KEY (`id`)
);

DROP TABLE IF EXISTS `t_movie`;

CREATE TABLE `t_movie` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '唯一id',
  `name` varchar(80) NOT NULL DEFAULT '' COMMENT '名字',
  `type` varchar(20) NOT NULL DEFAULT '' COMMENT '类型',
  `img_url` varchar(300) NOT NULL DEFAULT '' COMMENT '封面',
  `description` varchar(255) NOT NULL DEFAULT '' COMMENT '描述',
  `director` varchar(55) NOT NULL DEFAULT '' COMMENT '导演',
  `actors` varchar(255) NOT NULL DEFAULT '' COMMENT '演员',
  `release_date` datetime DEFAULT NULL COMMENT '上映时间',
  `duration` int DEFAULT NULL COMMENT '时长',
  PRIMARY KEY (`id`)
);
