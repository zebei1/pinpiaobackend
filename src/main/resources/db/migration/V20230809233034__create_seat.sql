DROP TABLE IF EXISTS `t_seat`;

CREATE TABLE `t_seat` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '唯一id',
  `session_id` int NOT NULL COMMENT '场次id',
  `seat_row` int NOT NULL COMMENT '行',
  `seat_col` int NOT NULL COMMENT '列',
  `seat_number` varchar(55)  DEFAULT NULL COMMENT '座位号',
  `status` tinyint DEFAULT NULL COMMENT '可选，已选，已售',
  PRIMARY KEY (`id`)
);
